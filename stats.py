import matplotlib.pyplot as plt
import random as rnd

class Stats:
    def __init__(self):
        self.data = dict()
        self.count = dict()

    def add(self, key, elem):
        if key not in self.data:
            self.data[key] = {elem: 1}
            self.count[key] = 1
        elif elem not in self.data[key]:
            self.data[key][elem] = 1
            self.count[key] += 1
        else:
            self.data[key][elem] += 1
            self.count[key] += 1

    def generate_one(self, key):
        ind = rnd.randint(0, self.count[key] - 1)
        for k, v in self.data[key].items():
            if ind < v:
                return k
            ind -= v
    
    def generate_counters(self):
        result = dict()
        for key in self.data.keys():
            if key.endswith('count'):
                result[key] = self.generate_one(key)
        return result

    def visualize(self, key):
        keys = sorted(self.data[key].keys())
        values = [self.data[key][k] for k in keys]
        plt.bar(keys, values)
        plt.title(label=key)
        plt.xlabel('No. of experiment')
        plt.ylabel('Count')
        plt.grid()
        plt.show()

    def visualize_all(self):
        fig, axes = plt.subplots(2, 4)
        ind = 0
        print(len(self.data))
        for stat in self.data.keys():
            x = ind // 4
            y = ind % 4
            keys = self.data[stat].keys()
            values = [self.data[stat][key] for key in keys]
            axes[x, y].set(title=stat)
            axes[x, y].bar(keys, values)
            axes[x, y].grid()
            ind += 1
        plt.show()


class RouteNumberInfo:
    def calc_route_stats(self, route):
        counts = {'cycle_count' : 0, 'inversion_count' : 0, 'path_count' : 0, \
                    'transition_count' : 0, 'return_count' : 0}
        feeders = ['А1', 'А2']
        next_feeder = 'А2'
        last_sym = 0
        ind = 0
        last_entry = dict()
        mask = ''
        flag = False
        while ind < len(route):
            pos = ind
            if ind > 0 and route[ind] in feeders:
                mask += 'p'
                counts['path_count'] += 1
                self.stats.add('path_len', ind - last_sym + 1)
                # print("path: ", route[last_sym : ind + 1])
                last_sym = ind
                last_entry[route[ind]] = ind
                if route[ind] == next_feeder:
                    counts['transition_count'] += 1
                    next_feeder = 'А2' if next_feeder == 'А1' else 'А1'
                    mask += 'T'
                else:
                    counts['return_count'] += 1
                    mask += 'R'
                ind += 1
                continue
            if route[ind] in last_entry and last_entry[route[ind]] == ind - 2 and last_sym != ind - 1:
                counts['inversion_count'] += 1
                inv_len = 0
                mid = ind - 1
                while mid >= inv_len and mid + inv_len < len(route) and route[mid - inv_len] == route[mid + inv_len] and route[mid - inv_len] not in feeders:
                    inv_len += 1
                inv_len -= 1
                if last_sym != mid:
                    counts['path_count'] += 1
                    self.stats.add('path_len', mid - last_sym + 1)
                    mask += 'p'
                # if len(mask) > 0 and mask[-1] == 'i':
                #     flag = True
                # mask += 'i'
                self.stats.add('inversion_len', inv_len)
                mask += 'i'
                # if last_sym != mid:
                #     print("path: ", route[last_sym : mid + 1])
                # print("inv: ", route[mid + 1 : mid + inv_len + 1])
                ind += inv_len
                last_sym = ind - 1
            elif route[ind] in last_entry and last_entry[route[ind]] >= last_sym:
                counts['cycle_count'] += 1
                if last_sym != last_entry[route[ind]]:
                    counts['path_count'] += 1
                    self.stats.add('path_len', last_entry[route[ind]] - last_sym + 1)
                    mask += 'p'
                # if mask[-1] == 'c':
                #     flag = True
                mask += 'c'
                self.stats.add('cycle_len', ind - last_entry[route[ind]] + 1)
                # if last_sym != last_entry[route[ind]]:
                    # print("path: ", route[last_sym : last_entry[route[ind]] + 1])
                # print("cycle: ", route[last_entry[route[ind]] : ind + 1])
                last_sym = ind
                ind += 1
            else:
                ind += 1
            last_entry[route[pos]] = pos
        # if last_sym != len(route) - 1:
            # print("path: ", route[last_sym:])genereated_route
        if last_sym != len(route) - 1:
            counts['path_count'] += 1
            self.stats.add('path_len', len(route) - last_sym)
            mask += 'p'
        for key, value in counts.items():
            self.stats.add(key, value)

    def __init__(self, routes):
        self.stats = Stats()
        for route in routes:
            self.calc_route_stats(route)

    def generate_one(self, key):
        return self.stats.generate_one(key)

    def generate_counters(self):
        return self.stats.generate_counters()

    def visualize(self, key):
        self.stats.visualize(key)

    def visualize_all(self):
        self.stats.visualize_all()