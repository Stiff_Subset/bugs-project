routes = open("routes.txt", "r")
vertices = set()
bad_edges = set()
bugs_count = int(routes.readline())
for i in range(bugs_count):
    cnt = int(routes.readline())
    for j in range(cnt):
        line = routes.readline().split()
        length = len(line)
        for k in range(1, length):
            vertices.add(line[k])
            if k != length - 1 and line[k] == line[k + 1]:
                bad_edges.add((line[k], line[k + 1]))
print(vertices)
print(bad_edges)