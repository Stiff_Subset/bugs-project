from generator import generate_routes, add_edges
from levenshtein import levenshtein_distance
from stats import RouteNumberInfo
from time import time

import matplotlib.pyplot as plt
import random as rnd
import numpy as np

MAX_ROUTE_NUM = 9
MIN_LENGTH = 5
GENERATED_ROUTES_NUM = 11

def parse_routes():
    routes = open("routes.txt", "r")
    bugs_count = int(routes.readline())
    routes_by_number = [[] for _ in range(MAX_ROUTE_NUM)]
    for i in range(bugs_count):
        lines = int(routes.readline())
        for j in range(lines):
            route_info = routes.readline().split()
            route_num = int(route_info[0])
            route_info.remove(route_info[0])
            if len(route_info) < MIN_LENGTH or route_num > MAX_ROUTE_NUM:
                continue
            add_edges(route_info)
            for k in range(len(route_info) - 1, -1, -1):
                if route_info[k] in ['А1', 'А2']:
                    route_info = route_info[:k + 1]
                    break
            routes_by_number[route_num - 1].append(route_info)
    routes_info = []
    for i in range(MAX_ROUTE_NUM):
        routes_info.append(RouteNumberInfo(routes_by_number[i]))
    return routes_info, routes_by_number


def main():
    rnd.seed(time())
    routes_info, routes_by_number = parse_routes()
    min_distances = []
    mean_distances = []
    median_distances = []
    for i in range(len(routes_info)):
        generated_routes = generate_routes(routes_info[i], GENERATED_ROUTES_NUM)
        distances = []
        for generated_route in generated_routes:
            min_dist = 10000000
            for route in routes_by_number[i]:
                min_dist = min(min_dist, levenshtein_distance(route, generated_route))
            distances.append(min_dist)
        distances = sorted(distances)
        min_distances.append(min(distances))
        mean_distances.append(sum(distances) / len(distances))
        median_distances.append(distances[GENERATED_ROUTES_NUM // 2])

    xaxis = np.arange(MAX_ROUTE_NUM) + 1
    plt.plot(xaxis, min_distances)
    plt.plot(xaxis, mean_distances)
    plt.plot(xaxis, median_distances)
    plt.legend(title='Stats', labels=[
        'Min distance',
        'Mean distance', 
        'Median distance'
    ])
    plt.xlabel('No. of experiment')
    plt.ylabel('Levenshtein distance')
    plt.grid()
    plt.show()


if __name__ == '__main__':
    main()