import random as rnd

MAX_STEPS = 20

graph = dict()

def add_edges(route):
    feeders = ['А1', 'А2']
    for i in range(len(route) - 1):
        u = route[i]
        v = route[i + 1]
        if u in feeders or v in feeders:
            continue
        if not u in graph:
            graph[u] = {v : 1}
        elif v not in graph[u]:
            graph[u][v] = 1
        else:
            graph[u][v] += 1


def get_next(path):
    count = 0
    ver = path[-1]
    for key, value in graph[ver].items():
        if key not in path:
            count += value
    if count == 0:
        return None
    ind = rnd.randint(0, count - 1)
    for key, value in graph[ver].items():
        if key not in path and ind < value:
            return key
        ind -= value
    return None


def generate_path(prev, start, length, to_o=False):
    if start in ['А1', 'А2']:
        return None
    path = [prev, start]
    for i in range(length - 1):
        path.append(get_next(path))
        if path[-1] is None:
            return None
        if to_o and path[-1] == 'О':
            return path[:2]
    return path[2:]


def generate_inversion(path, length):
    if len(path) <= length + 1:
        return None
    for i in range(len(path) - 1, len(path) - length - 1, -1):
        if path[i] in ['А1', 'А2']:
            return None
    path.extend(path[-2:len(path) - length - 2:-1])
    return path


def generate_cycle(prev, start, length):
    for i in range(MAX_STEPS):
        path = generate_path(prev, start, length - 1)
        if path is not None and start in graph[path[-1]]:
            break
    if path is None or start not in graph[path[-1]]:
        return None
    path.append(start)
    return path


def generate_partition(value, count):
    if count == 0:
        return [0, value]
    result = [1 for _ in range(count)]
    value -= count
    if value < 0:
        return None
    for i in range(count):
        add = rnd.randint(0, value)
        result[i] += add
        if i > 0:
            result[i] += result[i - 1]
        value -= add
    result[-1] += value
    return [0].extend(result)


def shuffle_string(str):
    str_list = list(str)
    rnd.shuffle(str_list)
    return ''.join(str_list)


def shuffle_submask(submask):
    for i in range(MAX_STEPS):
        submask = shuffle_string(submask)
        if submask.find('pp') == -1 and submask[0] != 'p' and submask[-1] != 'p':
            return submask
    return None


def generate_mask(stats):
    mask = 'i' * stats['inversion_count'] + 'c' * stats['cycle_count']
    sep = stats['transition_count'] + stats['return_count'] - 1
    if sep < 0:
        return None
    mask += 'p' * (stats['path_count'] - 2 * sep)
    partition = generate_partition(len(mask), sep)
    if partition is None:
        return None
    for i in range(MAX_STEPS):
        mask = shuffle_string(mask)
        submasks = [mask[partition[i]:partition[i + 1]] for i in range(len(partition) - 1)]
        flag = True
        for j in range(len(submasks)):
            submasks[j] = shuffle_submask(submasks[j])
            if submasks[j] is None:
                flag = False
                break
        if not flag:
            continue
        mask = ''
        ends = 'R' * stats['return_count'] + 'T' * stats['transition_count']
        ends = list(ends)
        for submask in submasks:
            mask += 'p' + submask + 'p' + ends[-1]
            ends.pop()
        return mask
    return None


def extend_route(info, path, operator):
    if operator == 'i':
        return generate_inversion(path, info.generate_one('inversion_len'))
    elif operator == 'c':
        return generate_cycle(path[-2], path[-1], info.generate_one('cycle_len'))
    elif operator == 'p':
        return generate_path(path[-2], path[-1], info.generate_one('path_len'))
    else:
        raise RuntimeError('Unexpected character')


def generate_route(info, mask):
    result = ['А1', 'О']
    cur_feeder = 'А1'
    next_feeder = 'А2'
    for i in range(len(mask)):
        if mask[i] == 'p' and i + 1 < len(mask) and mask[i + 1] in 'RT':
            if result[-1] == 'О':
                continue
            augment = None
            for j in range(MAX_STEPS):
                augment = generate_path(result[-2], result[-1], 100, True)
                if augment is not None:
                    break
            if augment is None:
                return None
            result.extend(augment)
            continue
        if mask[i] in 'RT':
            result.append(cur_feeder if mask[i] == 'R' else next_feeder)
            if mask[i] == 'T':
                cur_feeder, next_feeder = next_feeder, cur_feeder
            if i != len(mask) - 1:
                result.append('О')
        else:
            augment = None
            for j in range(MAX_STEPS):
                augment = extend_route(info, result, mask[i])
                if augment is not None:
                    break
            if augment is None:
                return None
            result.extend(augment)
    return result


def generate_routes(info, count):
    result = []
    while (count > 0):
        stats = info.generate_counters()
        mask = generate_mask(stats)
        if mask is None:
            continue
        route = generate_route(info, mask)
        if route is not None:
            result.append(route)
            count -= 1
    return result